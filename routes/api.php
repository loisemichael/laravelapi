<?php

use App\Http\Controllers\API\KaryawanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('karyawan',[KaryawanController::class,'index']);
Route::get('karyawan/{id}',[KaryawanController::class,'show']);
Route::post('karyawan',[KaryawanController::class,'store']);
Route::post('karyawan/{id}',[KaryawanController::class,'update']);
Route::delete('karyawan/{id}',[KaryawanController::class,'destroy']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
