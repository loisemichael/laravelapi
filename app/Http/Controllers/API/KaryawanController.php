<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Karyawan;
use Exception;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawan = Karyawan::all();
        if($karyawan){
            return ResponseFormatter::createResponse(200,'Success',$karyawan);
        }else{
            return ResponseFormatter::createResponse(400,'Failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $karyawanData = $request->validate([
                'nama' => 'required',
                'alamat' => 'required',
                ]
            );

            $karyawan = Karyawan::create($karyawanData);

            $data = Karyawan::where('id','=',$karyawan->id)->get();
            
            if($data){
                return ResponseFormatter::createResponse(200,'Success Add',$data);
            }else{
                return ResponseFormatter::createResponse(400,'Failed');
            }

        } catch (Exception $ex) {
            return ResponseFormatter::createResponse(400,'Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $data = Karyawan::where('id','=',$id)->get();
            
            if($data){
                return ResponseFormatter::createResponse(200,'Success',$data);
            }else{
                return ResponseFormatter::createResponse(400,'Failed');
            }

        } catch (Exception $ex) {
            return ResponseFormatter::createResponse(400,'Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $karyawanData = $request->validate([
                'nama' => 'required',
                'alamat' => 'required',
                ]
            );

            $karyawan = Karyawan::findorfail($id);


            $karyawan->update($karyawanData);

            $data = Karyawan::where('id','=',$karyawan->id)->get();
            
            if($data){
                return ResponseFormatter::createResponse(200,'Success Update',$data);
            }else{
                return ResponseFormatter::createResponse(400,'Failed');
            }

        } catch (Exception $ex) {
            return ResponseFormatter::createResponse(400,'Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            
        try{

            $karyawan = Karyawan::findorfail($id);

            $data = $karyawan->delete();
            
            $data = Karyawan::where('id','=',$karyawan->id)->get();

            if($data){
                return ResponseFormatter::createResponse(200,'Success Deleted',$data);
            }else{
                return ResponseFormatter::createResponse(400,'Failed');
            }

        } catch (Exception $ex) {
            return ResponseFormatter::createResponse(400,'Failed');
        }
    }
}
